    FROM registry.gitlab.com/actonatedev/docker/ubuntu:node-env
    #ARG user
    #ARG pass
    # Install wget 
    RUN apt-get -y install wget
    # Install flyway commandline
    RUN wget "https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/5.1.1/flyway-commandline-5.1.1-linux-x64.tar.gz"
    RUN tar -zxvf flyway-commandline-5.1.1-linux-x64.tar.gz
    ENV PATH=${PATH}:/flyway-5.1.1
    # Set dremora npm config
    RUN npm config set @actonate:registry http://dremora.actonatehost.com:1200
    # Own the .sh file and run it 
    ADD --chown=root:root https://gitlab.com/parthgandhi7/polkadot-docker-test/raw/master/file.sh /
    RUN ["chmod", "+x", "/file.sh"]
    ENTRYPOINT ["/file.sh"]