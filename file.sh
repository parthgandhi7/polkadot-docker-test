#!/bin/sh
mkdir ~/repos && cd ~/repos
git clone https://${user}:${pass}@gitlab.com/polkadot/polkadot-frontend.git
cd polkadot-frontend
npm i
npm i @actonate/mirkwood@latest
npm i @actonate/mirkwood-rx@latest
npm run build-dev
cd ..
git clone https://${user}:${pass}@gitlab.com/polkadot/polkadot-server.git
cd polkadot-server
npm i
npm i @actonate/mirkwood@latest
npm run build-dev
cd ..
git clone https://${user}:${pass}@gitlab.com/polkadot/polkadot-runner.git
cd polkadot-runner
git checkout dev
npm i
npm run build-dev
cd /
git clone https://${user}:${pass}@gitlab.com/polkadot/polkadot-migrations.git
flyway -url=jdbc:postgresql://${dburl} -user=postgres migrate -locations=filesystem:/polkadot-migrations -password=123
/bin/bash