Arguments to be passed while starting the docker container are as follows:
```
SSH_PRIVATE_KEY - Any Valid SSH private key which has access to the polkadot repos
```

```
couchdburl -  A valid couchdb url (with/without username password) which has 
polkadot-dev (or polkadot-prod as per the requirement) db with the project specific
.env and env-dev.json (or env.prod.json) files.
E.g: http://139.162.23.51:5984 or http://{username}:{password}@139.162.23.51:5984
```

```
projectname - A valid project name 
E.g.: mybageecha
```

```
dburl - A postgres dburl where migrations are to be performed
E.g.: host:port/database
```