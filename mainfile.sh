#!/bin/sh
# Assign a static private key through command line args
mkdir /root/.ssh/
echo "${SSH_PRIVATE_KEY}" > /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa
touch /root/.ssh/known_hosts
ssh-keyscan gitlab.com >> /root/.ssh/known_hosts
mkdir ~/repos && cd ~/repos
# Clone polkadot frontend
git clone git@gitlab.com:polkadot/polkadot-frontend.git
cd polkadot-frontend
npm i
npm run build-dev
# Get .env and pm2 env file from couch db with the provided couchdb url
wget ${couchdburl}/polkadot-dev/${projectname}_frontend_env/.env
wget ${couchdburl}/polkadot-dev/${projectname}_frontend_env/env-dev.json
pm2 start env-dev.json
cd ..
# Clone polkadot server
git clone git@gitlab.com:polkadot/polkadot-server.git
cd polkadot-server
npm i
npm run build-dev
wget ${couchdburl}/polkadot-dev/${projectname}_server_env/.env
wget ${couchdburl}/polkadot-dev/${projectname}_server_env/env-dev.json
pm2 start env-dev.json
cd ..
# Clone polkadot runner
git clone git@gitlab.com:polkadot/polkadot-runner.git
cd polkadot-runner
git checkout dev
npm i
npm run build-dev
wget ${couchdburl}/polkadot-dev/${projectname}_runner_env/env-dev.json
pm2 start env-dev.json
cd /
# Clone polkadot migrations
git clone git@gitlab.com:polkadot/polkadot-migrations.git
# Run migration on dburl specified
flyway -url=jdbc:postgresql://${dburl} -user=postgres migrate -locations=filesystem:/polkadot-migrations -password=123
/bin/bash